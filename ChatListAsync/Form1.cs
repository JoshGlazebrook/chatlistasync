﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatListAsync
{
    public partial class Form1 : Form
    {
        ChatList Channels;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Channels = new ChatList();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            listViewChannels.Items.Clear();
            var items = await Channels.GetChatListJSON();
            //var items = Channels.GetChatListJSON40();
            
            if (items != null)
            {
                listViewChannels.BeginUpdate();
                items.ForEach((channel) =>
                {
                    var item = new ListViewItem(channel.roomname);
                    item.SubItems.Add(channel.usercount.ToString());
                    item.SubItems.Add(channel.topic);
                    listViewChannels.Items.Add(item);
                });
                listViewChannels.EndUpdate();
            }
        }
    }
}
