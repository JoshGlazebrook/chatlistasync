﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace ChatListAsync
{
    class ChatList
    {
        HttpClient client;

        public ChatList()
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 10);
            client.BaseAddress = new Uri("http://winmxunlimited.net");
        }

        public async Task<List<ChatRoom>> GetChatListJSON()
        {
            try
            {
                var res = await client.GetAsync("/api/chatroomlist/v1/");
                if (res.IsSuccessStatusCode)
                {
                    var result = await res.Content.ReadAsAsync<List<ChatRoom>>();
                    return result;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // .NET 4.0 method
        public List<ChatRoom> GetChatListJSON40()
        {
            var wc = new WebClient();
            try
            {
                var json = wc.DownloadString("http://winmxunlimited.net/api/chatroomlist/v1");
                var result = JsonConvert.DeserializeObject<List<ChatRoom>>(json);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }


    class ChatRoom
    {
        public string roomname { get; set; }
        public int usercount { get; set; }
        public int userlimit { get; set; }
        public string topic { get; set; }
        public string extra { get; set; }
        public string mxlnk { get; set; }

        public ChatRoom() { }
    }
}
